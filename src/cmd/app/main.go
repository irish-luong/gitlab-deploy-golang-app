package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("Started on 8090")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Get a response")

		fmt.Fprintf(w, "Hi %s\n", r.URL.Path)

		fmt.Println("=====")
	})

	http.HandleFunc("/health_check", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "ok")
	})

	http.ListenAndServe(":8090", nil)

}
