package main

import (
	"gitlab/util"
	"testing"
)

func TestHello(t *testing.T) {
	result := util.Hello("Hello")
	if result != "Hello" {
		t.Errorf("Output expect is Hello")
	}
}
